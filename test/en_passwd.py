import hashlib

shared_private_key = "ABCDEF"

def create_signature(data):
    return hashlib.sha1(repr(data) + "," + shared_private_key).hexdigest()

def verify_signature(data, signature):
    return signature == create_signature(data)

print verify_signature("HELLO", create_signature("HELLO") )