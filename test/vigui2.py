#!/usr/bin/env python

import curses, sys, time
from viconfig import ReadConfig
from vissh import SSHConnect

class VISSHCurses():
    
    def __init__(self):
        
        self.win = curses.initscr()
        curses.noecho()
        #curses.cbreak()
    
    def show_term(self):
    
        self.win.border(0)
        self.win.addstr(1, 3, "ViSSH Manager | c:connect | e:edit | a:add | d:delete | q:quit | r:refresh screen |")
        status_y = 58
        status_x = 3
        status_msg_pos = status_x+8
        #self.win.addstr(status_y, status_x, "Status:")
        self.win.refresh()
        
        main_y = 2; main_x = 2
        self.main = self.curses.newwin(56,115,2,3)
        self.main.border(0)
        self.main.keypad(1)
        self.main.addstr(main_y,main_x,"Started ViSSH Manager")
        self.main.refresh()
        
        side_x = 2; side_y = 3
        side_hosts_pos=4
        side = self.curses.newwin(56,20,2,120)
        side.border(0)
        side.keypad(1)
        side.refresh
        
        read = ReadConfig(None, '../conf/vissh.cfg')
        read.getAllHost()
        
        side.addstr(1,2,"Hosts")
        side.addstr(3,2,"<new hostname>")
        
        host_num = {}; host_num.update({4 : 'Press "A" key to add new hostname'})
        for x in read.all:   
            side.addstr(side_hosts_pos,2,x)
            side.refresh()
            side_hosts_pos+=1
            host_num.update({side_hosts_pos : x})
        
        cusor_pos = side.getch(side_y,side_x)
        side.refresh()
        
        while True:
            
            cursor_y, cursor_x = side.getyx()
            
            if cursor_y == side_hosts_pos or cursor_y == 2:
                side_x = 2; side_y = 3
                side.getch(side_y,side_x)
                cursor_y, cursor_x = side.getyx()
                
            self.main.refresh()
            side.refresh()
            
            hostname = host_num[cursor_y+1]
            host_domain = "No Host"
            
            key_input = side.getch()
            if key_input == ord("q"):
                self.win.addstr(status_y,status_msg_pos, "Quitting App")
                self.win.refresh(); time.sleep(1); sys.exit()
            
            if key_input == self.curses.KEY_DOWN:
                side_y = side_y +1; side.move(side_y,side_x)
            
            if key_input == self.curses.KEY_UP:
                side_y = side_y -1; side.move(side_y,side_x)
            
            if key_input == ord("c") or key_input: #== curses.KEY_ENTER:
                main_y = main_y +1
                get_host = ReadConfig(hostname, '../conf/vissh.cfg')
                get_host.getSingleHost()
                host_domain = get_host.host
                self.main.addstr(main_y,main_x,"Connecting to SSH Host --> " + hostname + " (" + host_domain + ")")
                self.main.refresh()
                connect = SSHConnect(get_host.host,get_host.user,get_host.password,get_host.port,get_host.key)
                connect.curses_win(self.win, status_y, status_msg_pos)
                connect.ssh_cmdln()
                
            if key_input == ord("e"):
                main_y = main_y +1
                self.main.addstr(main_y,main_x,"Edit SSH Host --> " + hostname)
            
            if key_input == ord("d"):
                main_y = main_y +1
                self.main.addstr(main_y,main_x,"Delete SSH Host --> " + hostname)
            
            if key_input == ord("a"):
                main_y = main_y +1
                self.main.addstr(main_y,main_x,"Adding New SSH Host")
            
#             if key_input == ord("r"):
#                 main_y = main_y +1
#                 main.addstr(main_y,main_x,"Rebuild Screen")
#                 rebuild_scr()
#                 
#         def rebuild_scr(self):
#             curses.wrapper(show_term)
    
if __name__=='__main__':
    show_term = VISSHCurses()
    curses.wrapper(show_term.show_term())