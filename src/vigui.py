#!/usr/bin/env python

import curses, curses.panel
import time
import sys

from util.viconfig import ReadConfig, DeleteHost
import viconstants as CONS
from util import util as UTIL


def show_term(self):
    
    sizes = UTIL.ViSSHUtil()
    sizes.get_term_size()
    main_size_y, main_size_x = sizes.main_win_size()
    side_size_y, side_size_x = sizes.side_win_size()

    win = curses.initscr()
    win.border(0)
    win.addstr(1, 3, CONS.MAIN_TOP_MENU)
    status_y = main_size_y + 2
    status_x = 3
    status_msg_pos = status_x + 8
    win.addstr(status_y, status_x, "Status:")
    win.refresh()

    """Main display window positions"""
    main_dis_pos_y = 2
    main_dis_pos_x = 2
    
    """Example: curses.newwin(nlines, ncols, begin_y, begin_x)."""
    main_dis = curses.newwin(main_size_y, main_size_x, 2, 3)
    main_dis.border(0)
    main_dis.keypad(1)
    main_dis.addstr(main_dis_pos_y, main_dis_pos_x, CONS.START_LOG_MESSAGE)
    main_dis.refresh()
    
    """ Side window positions"""
    side_dis_pos_y = 3
    side_dis_pos_x = 2
    side_nav_pos = 4
    
    """Example: curses.newwin(nlines, ncols, begin_y, begin_x)."""
    side_dis = curses.newwin(side_size_y, side_size_x, 2, main_size_x - 2)
    side_dis.border(0)
    side_dis.keypad(1)
    side_dis.refresh
    side_dis.addstr(1, 2, "Hosts")
    side_dis.addstr(3, 2, "<new hostname>")

    read = ReadConfig(None, CONS.CONFIG_FILE)
    read.getAllHost()

    side_host_nav_num = {};
    side_host_nav_num.update({4: 'Press "A" key to add new hostname'})
    
    for hosts in read.all:
        side_dis.addstr(side_nav_pos, 2, hosts)
        side_dis.refresh()
        side_nav_pos += 1
        side_host_nav_num.update({side_nav_pos: hosts})

    side_dis.getch(side_dis_pos_y, side_dis_pos_x)
    side_dis.refresh()
    
    while True:
        
        try:
            cursor_y = side_dis.getyx()[0]
            
            if cursor_y == side_nav_pos or cursor_y == 2:
                side_dis_pos_x = 2
                side_dis_pos_y = 3
                side_dis.getch(side_dis_pos_y, side_dis_pos_x)
                cursor_y = side_dis.getyx()[0]
    
            hostname = side_host_nav_num[cursor_y + 1]
            host_domain = "No Host"
    
            key_input = side_dis.getch()
            
            if key_input == ord("q"):
                win.addstr(status_y, status_msg_pos, "Quitting App")
                win.refresh()
                time.sleep(1)
                sys.exit()
            
            """ Key down pressed do this"""
            if key_input == 258: #curses.KEY_DOWN:
                #main_dis_pos_y += 1
                #main_dis.addstr(main_dis_pos_y, main_dis_pos_x, "Key down pressed"); main_dis.refresh()
                side_dis_pos_y += 1;
                side_dis.move(side_dis_pos_y, side_dis_pos_x)
                
            """ Key up pressed do this"""
            if key_input == 259: #curses.KEY_UP:
                side_dis_pos_y -= 1;
                side_dis.move(side_dis_pos_y, side_dis_pos_x)
    
            if key_input == ord("c"): #or [curses.KEY_ENTER, ord('\n')]:
    
                from vissh import SSHConnect
                
                main_dis_pos_y += 1
                get_host = ReadConfig(hostname, CONS.CONFIG_FILE)
                get_host.getSingleHost()
                host_domain = get_host.host
                main_dis.addstr(main_dis_pos_y, main_dis_pos_x, "Connecting to SSH Host --> " + hostname + " (" + host_domain + ")")
                main_dis.refresh()
                connect = SSHConnect(get_host.host, get_host.user, get_host.password, get_host.port, get_host.key)
                connect.curses_win(win, status_y, status_msg_pos)
                connect.ssh_cmdln()
    
            if key_input == ord("e"):
                main_dis_pos_y += 1
                main_dis.addstr(main_dis_pos_y, main_dis_pos_x, "Edit SSH Host --> " + hostname)
    
            if key_input == ord("d"):
                main_dis_pos_y += 1
                
                main_dis.addstr(main_dis_pos_y, main_dis_pos_x, "Delete SSH Host --> " + hostname); main_dis.refresh()
                
                if delete_dialog(status_y, status_msg_pos, win) is True:
                    delete_host = DeleteHost(hostname, CONS.CONFIG_FILE)
                    
                    if delete_host.del_section() is True:
                        print_status(win,"Deleted" + hostname)
                        #win.addstr(status_y, status_msg_pos, "Deleted host section [" + hostname + "]")
                    else:
                        win.addstr(status_y, status_msg_pos, "Error deleting [" + hostname + "]")
                
                rebuild_scr()

            if key_input == ord("a"):
                main_dis_pos_y += 1
                main_dis.addstr(main_dis_pos_y, main_dis_pos_x, "Adding New SSH Host")
    
            if key_input == ord("r"):
                win.clear()
                rebuild_scr()
        
        except KeyboardInterrupt:
            win.clear()
            rebuild_scr()
        

def rebuild_scr():

    curses.wrapper(show_term)
    

def print_status(win, msg):
    
    term_size = UTIL.ViSSHUtil()
    term_size.get_term_size()
    status_pos_y, status_pos_x = term_size.status_win_size()
    
    #print status_pos_y, status_pos_x
    
    """WINDOW *newwin(int nlines, int ncols, int begin_y, int begin_x);"""
    ##status_win_dis = curses.newwin(1, status_pos_x, status_pos_y, 2)
    status_win_dis = curses.newwin(2, 232, 57, 2)
    
    status_message = "Status: "+msg
    status_win_dis.addstr(1,1,status_message)
    ##status_win_dis.move(2,2)
    ##status_win_dis.border(0)    
    
    pnl = curses.panel
    pnl.new_panel(status_win_dis)
    pnl.top_panel()
    #panel.move(10,10)
    #move(status_pos_y, status_pos_x)
    pnl.update_panels()
    
    status_win_dis.refresh()
    
    #time.sleep(20)
    
    return


def delete_dialog(pos_y, pos_x, win):
     
    win.addstr(pos_y, pos_x, "Delete? (y/n)  ")
    win.refresh()
    key_input = win.getch()
    
    while True:
    
        if key_input == ord("y"):return True
        else: break


def refresh_all_windows():
    
    return
