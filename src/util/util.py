#!/usr/bin/python

from subprocess import check_output as SubProcessChecker
from shlex import split as SplitOutput


class ViSSHUtil():

    def __init__(self):

        self.term_y, self.term_x = SubProcessChecker(SplitOutput('stty size')).split()

    def get_term_size(self):

        return self.term_y, self.term_x

    def main_win_size(self):
        
        main_y = int(self.term_y) - 5
        main_x = int(self.term_x) * 80 / 100   
        return main_y, main_x
    
    def side_win_size(self):
        
        side_y = int(self.term_y) - 5
        side_x = int(self.term_x) * 20 / 100
        return side_y, side_x
    
    def status_win_size(self):
        
        status_y = int(self.term_y) + 1
        status_x = int(self.term_x) - 6
        return status_y, status_x 
