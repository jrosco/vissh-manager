#!/usr/bin/python

import ConfigParser, sys


class WriteConfig():
    
    def __init__(self, file, heading, host, port, user, passwd, key ):
        
        self.file = file
        self.heading = heading
        self.host = host
        self.port = port
        self.user = user
        self.passwd = passwd
        self.key = key

    def writeNewSection(self):  
        
        config = ConfigParser.RawConfigParser()

        config.add_section(self.heading)
        config.set(self.heading, 'host', self.host)
        config.set(self.heading, 'port', self.port)
        config.set(self.heading, 'user', self.user)
        config.set(self.heading, 'password', self.passwd)
        config.set(self.heading, 'key', self.key)
        
        return
           
           
class ReadConfig():
    
    def __init__(self, heading, config_file):

        self.heading = heading
        self.file = config_file
        self.config = ConfigParser.SafeConfigParser()
        self.config.readfp(open(self.file))

    def getSingleHost(self):
        
        try:
            self.host = self.config.get(self.heading, 'host')
            self.port = self.config.get(self.heading, 'port')
            self.user = self.config.get(self.heading, 'user')
            
            if self.config.get(self.heading, 'password'):
                self.password =  self.config.get(self.heading, 'password')
                
            if self.config.get(self.heading, 'key'):
                self.key = self.config.get(self.heading, 'key')
                
            return self.password, self.key, self.host, self.port, self.user
        
        except ConfigParser.NoSectionError:
            return False

    def getAllHost(self):
         
        self.all = self.config.sections()
        self.all.sort()
        return
    
    
class DeleteHost():
    
    def __init__(self, section, config_file):
        
        self.section = section
        self.file = config_file
        self.delete_section = ConfigParser.SafeConfigParser()
        self.delete_section.read(self.file) 

    def del_option(self, option):
        
        try:
            self.delete_section.remove_option(self.section, option)
            new_file = open(self.file,"w")
            self.delete_section.write(new_file)
            return True
        except ConfigParser.NoSectionError:
            return False
    
    def del_section(self):
        
        try:
            self.delete_section.remove_section(self.section)
            new_file = open(self.file,"w")
            self.delete_section.write(new_file)
            return True
        except ConfigParser.NoSectionError:
            return False
        

#get_host = ReadConfig("Foo Bar2", "./vissh.cfg")
#get_host.getSingleHost()

#delete = DeleteHost("Foo Bar", "./vissh.cfg")
#delete.del_option("password")