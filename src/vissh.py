#!/usr/bin/python

import curses
import subprocess
from vigui import show_term
import viconstants as CONS


class SSHConnect():

    def __init__(self, server, user, passwd, port, key):
        
        self.server = server
        self.user = user
        self.passwd = passwd
        self.port = port
        self.key = key
        
    def connect(self):
        
        import pxssh
        ssh = pxssh.pxssh()
    
        try:
            ssh.login(self.server, self.user, password=self.passwd, login_timeout=30)
            #ssh.sendline("cat /etc/motd")
            #ssh.prompt()
            #output =  ssh.before
            #self.win.addstr(self.pos_y,self.pos_x,"                                               ")
            self.win.delch(self.pos_y,self.pos_x)
            self.win.refresh()
            self.win.addstr(self.pos_y,self.pos_x, "Connected " + self.server)
            self.win.refresh()
        except Exception, e:
            self.win.addstr(self.pos_y, self.pos_x, "Error:", e)
            self.win.refresh()
    
    def curses_win(self, win, pos_y, pos_x):
        self.win = win 
        self.pos_y = pos_y
        self.pos_x = pos_x 
        #self.win.addstr(self.pos_y,self.pos_x,"                                               ")
        #self.win.delch(self.pos_y,100)
        self.win.refresh()
        
    def ssh_cmdln(self):
        
        user_server = self.user + "@" + self.server
        timeout = format(CONS.SSH_TIMEOUT)
        
        try:
            p = subprocess.Popen(['ssh', user_server, '-o ConnectTimeout='+timeout], #add timeout to a settings file
                                 #stdin=subprocess.STDOUT,
                                 #stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,  
                                 shell=False)
            
            p.communicate()
            rc = p.returncode
            
            #if rc == 0:
            self.win.clear()
            curses.wrapper(show_term)
            self.win.refresh()
            #else:
            self.win.addstr(self.pos_y,self.pos_x,"Error Code:"+ str(rc))
            self.win.refresh()
            
        except Exception, e:
            error = format(e)
            self.win.addstr(self.pos_y, self.pos_x, "Error:" + error)
            self.win.refresh()
            
        #if p.wait():
        #    self.win.clear()
        #    return
